const physics = {
  "Vector2": function (x, y) {
    this.x = x;
    this.y = y;
    this.add = function (other) {
      return new physics.Vector2(this.x + other.x, this.y + other.y);
    };
    this.multiply = function (scalar) {
      return new physics.Vector2(this.x * scalar, this.y * scalar);
    };
    this.toString = function () {
      return "Vector2<" + this.x + "," + this.y + ">";
    };
  },

  "Collider": function (object) {
    this.isColliding = function (other) {
      return (
        object.position.x + object.scale.x > other.position.x &&
        object.position.y + object.scale.y > other.position.y &&
        object.position.x < other.position.x + other.scale.x &&
        object.position.y < other.position.y + other.scale.y
      );
    };
    this.isCollidingAny = function (others) {
      for (let i = 0; i < others.length; i++) {
        if (JSON.stringify(object) !== JSON.stringify(others[i]) && object.collider.isColliding(others[i])) {
          return true;
        }
      }
      return false;
    };
    this.bounce = function () {
      object.position = object.position.add(object.velocity.multiply(-object.speed));
    };
  }
}