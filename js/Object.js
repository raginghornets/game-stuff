const object = {
  "Object": function (x, y, w, h) {
    this.position = new physics.Vector2(x, y);
    this.scale = new physics.Vector2(w, h);
    this.velocity = new physics.Vector2(0, 0);
    this.collider = new physics.Collider(this);
    this.speed = 1;
    this.update = () => undefined;
    this.move = function () {
      this.position = this.position.add(this.velocity.multiply(this.speed));
    };
    this.draw = function (ctx) {
      ctx.fillStyle = "white";
      ctx.fillRect(this.position.x, this.position.y, this.scale.x, this.scale.y);
    };
  },

  "StaticObject": function (x, y, w, h) {
    let staticObject = new object.Object(x, y, w, h);
    staticObject.velocity = undefined;
    staticObject.move = () => undefined;
    staticObject.collider = undefined;
    return staticObject;
  },

  "Player": function (x, y, w, h) {
    let player = new object.Object(x, y, w, h);
    player.update = function () {
      this.velocity = new physics.Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    };
    return player;
  }
}